package me.justicepro.hotair.anticheat;

import java.time.LocalDateTime;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import me.justicepro.hotair.HotAir;
import me.justicepro.hotair.HotPlugin;

public class AntiCheat extends HotPlugin implements Listener
{
	
	public HashMap<Player, LocalDateTime> lastTeleport = new HashMap<>();
	public HashMap<Block, LocalDateTime> cancelledBlocks = new HashMap<>();
	
	@Override
	public void onEnable()
	{
		HotAir.registerPlugin(this);
		Bukkit.getPluginManager().registerEvents(this, this);
		
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent event)
	{
		double yDelta = event.getTo().getY() - event.getFrom().getY();
		double yVelocity = event.getPlayer().getVelocity().getY();
		boolean flying = event.getPlayer().isFlying();
		Player player = event.getPlayer();
		Block belowBlock = player.getLocation().getBlock().getRelative(0, -1, 0);
		
		if (getLastTeleport(player) != null)
		{
			if (getLastTeleport(player).isBefore(getLastTeleport(player).plusSeconds(4)))
			{
				// Cancel if player was recently teleported.
				return;
			}
		}
		
		if (flying)
		{
			return;
		}
		
		// Massive Y Movement without high velocity.
		if (yDelta > 5 && yVelocity < 5)
		{
			event.setCancelled(true);
		}
		
		// Y Movement without velocity.
		if (yDelta != 0 && yVelocity == 0)
		{
			event.setCancelled(true);
		}
		
		if (getCancelledBlocks(belowBlock) != null)
		{
			if (getCancelledBlocks(belowBlock).isBefore(getCancelledBlocks(belowBlock).plusSeconds(1)) && yDelta > 0)
			{
				event.setCancelled(true);
				return;
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlock(BlockPlaceEvent event)
	{
		if (event.isCancelled())
		{
			cancelledBlocks.put(event.getBlock(), LocalDateTime.now());
		}
	}
	
	@EventHandler
	public void onTeleport(PlayerTeleportEvent event)
	{
		lastTeleport.put(event.getPlayer(), LocalDateTime.now());
	}
	
	public LocalDateTime getLastTeleport(Player player)
	{
		if (!lastTeleport.containsKey(player))
		{
			return null;
		}
		return lastTeleport.get(player);
	}
	
	public LocalDateTime getCancelledBlocks(Block block)
	{
		if (!cancelledBlocks.containsKey(block))
		{
			return null;
		}
		return cancelledBlocks.get(block);
	}
	
}